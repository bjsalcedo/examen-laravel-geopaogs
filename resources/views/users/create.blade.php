@extends('app')

@section('content')
  <div class="row">
    <div class="col-md-10 col-md-offset-1">
      <div class="panel panel-default">
        <div class="panel-heading">
          <ul class="nav nav-tabs">
            <li>
              <a href="/user">
                Usuarios
              </a>
            </li>
            <li class="active">
              <a class="pull-right" href="#">
                Nuevo <span class="glyphicon glyphicon-plus"></span>
              </a>
            </li>
          </ul>
        </div>

        <div class="container-fluid">
          <div class="row">
            <div class="col-md-8 col-md-offset-2">
              <div class="panel panel-default">
                <div class="panel-heading">Nuevo usuario</div>
                @if (count($errors) > 0)
                  <div class="alert alert-danger">
                    <strong> Hubo algunos problemas.</strong><br><br>
                    <ul>
                      @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                      @endforeach
                    </ul>
                  </div>
                @endif
                <form method="post" action="{{ action('UserController@create_post') }}" class="form-horizontal">
                  {{ csrf_field() }}

                  <div class="form-group">
                    <label class="col-md-4 control-label">Usuario</label>
                    <div class="col-md-6">
                      <input name="usuario" type="text" class="form-control" required>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-md-4 control-label">Edad</label>
                    <div class="col-md-6">
                      <input name="edad" type="text" class="form-control" required>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-md-4 control-label">Clave</label>
                    <div class="col-md-6">
                      <input name="password" type="password" class="form-control" required>
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                      <input type="submit" value="Guardar" class="btn btn-primary">
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection