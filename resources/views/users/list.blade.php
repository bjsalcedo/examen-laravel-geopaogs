@extends('app')

@section('content')
  <div class="row">
    <div class="col-md-10 col-md-offset-1">
      <div class="panel panel-default">
        <div class="panel-heading">
          <ul class="nav nav-tabs">
            <li class="active">
              <a href="#">
                Usuarios
              </a>
            </li>
            <li>
              <a class="pull-right" href="/user/create">
                Nuevo <span class="glyphicon glyphicon-plus"></span>
              </a>
            </li>
          </ul>

        </div>

        <div class="panel-body">

          @if(Session::has('msj'))
            <div class="alert alert-{{Session::get('class')}}">
              <strong>{{Session::get('msj')}}</strong><br><br>
            </div>
          @endif

          <table class="table table-striped">
            <tr>
              <th>Codigo Usuario (ID)</th>
              <th>Usuario</th>
              <th>Edad</th>
            </tr>
            @foreach ($all_users as $user)
              <tr>
                <td>{{ $user->id }}</td>
                <td>{{ $user->usuario }}</td>
                <td>{{ $user->edad }}</td>
              </tr>
            @endforeach
          </table>
        </div>

      </div>
    </div>
  </div>
@endsection