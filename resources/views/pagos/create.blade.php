@extends('app')

@section('content')
  <div class="row">
    <div class="col-md-10 col-md-offset-1">
      <div class="panel panel-default">
        <div class="panel-heading">
          <ul class="nav nav-tabs">
            <li>
              <a href="/pay">
                Pagos
              </a>
            </li>
            <li class="active">
              <a class="pull-right" href="#">
                Nuevo <span class="glyphicon glyphicon-plus"></span>
              </a>
            </li>
          </ul>

        </div>

        <div class="panel-body">
        </div>
      </div>
    </div>
  </div>
@endsection