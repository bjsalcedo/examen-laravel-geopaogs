@extends('app')

@section('content')
  <div class="row">
    <div class="col-md-10 col-md-offset-1">
      <div class="panel panel-default">
        <div class="panel-heading">
          <ul class="nav nav-tabs">
            <li class="active">
              <a href="#">
                Pagos
              </a>
            </li>
            <li>
              <a class="pull-right" href="/pay/create">
                Nuevo <span class="glyphicon glyphicon-plus"></span>
              </a>
            </li>
          </ul>

        </div>

        <div class="panel-body">

          @if(Session::has('msj'))
            <div class="alert alert-{{Session::get('class')}}">
              <strong>{{Session::get('msj')}}</strong><br><br>
            </div>
          @endif

          <table class="table table-striped">
            <tr>
              <th>Codigo Pago (ID)</th>
              <th>Importe</th>
              <th>Fecha</th>
            </tr>
            @foreach ($all_pays as $pay)
              <tr>
                <td>{{ $pay->id }}</td>
                <td>{{ $pay->importe }}</td>
                <td>{{ $pay->fecha }}</td>
              </tr>
            @endforeach
          </table>
        </div>

      </div>
    </div>
  </div>
@endsection