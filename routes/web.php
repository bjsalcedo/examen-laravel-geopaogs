<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/user', 'UserController@list_user');
Route::get('/user/create', 'UserController@create_user');
Route::post('/user/create', 'UserController@create_post');
Route::get('/pay', 'PayController@list_pay');
Route::get('/pay/create', 'PayController@create_pay');
Route::post('/pay/create', 'PayController@create_post');