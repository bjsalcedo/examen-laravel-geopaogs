<?php

namespace App\Http\Controllers;

use App\Pay;
use Illuminate\Http\Request;

class PayController extends Controller
{
  public function list_pay() {
    $all_pays = Pay::all();
    return view('pagos.list', array('all_pays' => $all_pays));
  }

  public function create_pay() {
    return view('pagos.create');
  }

  public function create_post() {
    //
  }
}