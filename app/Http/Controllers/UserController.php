<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{
  public function list_user() {

    $all_users = User::all();
    return view('users.list', array('all_users' => $all_users));
  }

  public function create_user() {
    return view('users.create');
  }

  public function create_post(UserRequest $request) {
    $user     = User::create_post($request->all());
    if ($user){
      Session::flash('message', 'Registro exitoso.');
      Session::flash('class', 'success');
      return redirect()->to('/user');
    } else {
      Session::flash('message', 'Error al registrar los datos.');
      Session::flash('class', 'danger');
      return redirect()->back();
    }
  }
}