<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{

  public static function create_post($request) {
    $user              = new User;
    $user->usuario     = $request['usuario'];
    $user->edad        = $request['edad'];
    $user->password       = $request['password'];

    if ($user->save()) {
      return $user;
    }
  }
}